﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using System.IO;
using System.Runtime.Serialization.Formatters.Binary;
using System;

public class DataManager : MonoBehaviour
{

    #region segnature
    public static DataManager instance = null;
    private void Awake()
    {
        if (instance == null)
        {
            instance = this;
        }
        else
        {
            Debug.LogError("More than one DataManager!");
        }

    }
    #endregion

    public string dataFilePath = "/ItemList.dat";
    public List<ItemData> defaultItemsData;
    public GameObject contaner;
    public GameObject listItemPrefab;
    public InputField newTopicField;


    private List<ItemData> itemsData = null;

    void Start()
    {
        LoadData();

        ShowData();
    }

    public void SaveBtnClicked()
    {

        if (newTopicField.text != "")
        {
            ItemData newData = new ItemData
            {
                title = newTopicField.text
            };

            newTopicField.text = "";
            itemsData.Add(newData);
            SaveData();
            ShowData();
        }

    }
    public void SaveData()
    {
        if (itemsData != null)
        {
            FileStream fStream = null;
            try
            {
                BinaryFormatter formatter = new BinaryFormatter();
                fStream = File.Create(Application.persistentDataPath + dataFilePath);

                formatter.Serialize(fStream, itemsData);

            }
            catch (Exception e)
            {
                Debug.LogError(e.Message);
            }
            finally
            {
                if (fStream != null)
                {
                    fStream.Close();
                }
            }
        }
    }


    private void ShowData()
    {
        ClearList();
        if (itemsData != null)
        {
            foreach (ItemData data in itemsData)
            {
                GameObject Item = Instantiate(listItemPrefab);
                ListItem listItme = Item.GetComponent<ListItem>();

                listItme.itemData = data;

                listItme.Applay();

                Item.transform.SetParent(contaner.transform);
            }
        }
    }
    private void ClearList()
    {
        int counter = contaner.transform.childCount;

        for (int i = 0; i < counter; i++)
        {
            Destroy(contaner.transform.GetChild(i).gameObject);
        }
    }
    private void LoadData()
    {
        FileStream fStream = null;
        try
        {
            BinaryFormatter formatter = new BinaryFormatter();
            fStream = File.Open(Application.persistentDataPath + dataFilePath, FileMode.Open);

            itemsData = formatter.Deserialize(fStream) as List<ItemData>;

        }
        catch (Exception e)
        {
            Debug.Log(e.Message);

            itemsData = defaultItemsData;
        }
        finally
        {
            if (fStream != null)
            {
                fStream.Close();
            }
        }
    }

}
