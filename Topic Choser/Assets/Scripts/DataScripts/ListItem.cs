﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class ListItem : MonoBehaviour
{
    public ItemData itemData;
    public Text title;

    public void Applay()
    {
        title.text = itemData.title;
        GetComponent<Toggle>().isOn = itemData.isChecked;
    }
   
    public void Changed(bool value)
    {
        this.itemData.isChecked = value;
        DataManager.instance.SaveData();
    }
}
